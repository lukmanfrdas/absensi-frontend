import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: null,
    token: null
  },
  getters: {
    user() {
      let userData = JSON.parse(localStorage.getItem("userData"))
      return userData
    }
  },
  mutations: {
    setUser(state, value) {
      state.user = value.data
      state.token = value.token
      console.log('VAL', state.user)
    }
  }
})

export default store
