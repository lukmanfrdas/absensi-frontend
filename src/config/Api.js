import axios from "axios";
import router from '../routes/routes'
import Swal from 'sweetalert2';

console.log(123)

window.axios = axios
window.axios.defaults.baseURL = 'http://localhost:3001/api/'
window.axios.defaults.headers.common = {
    "Authorization": "Bearer " + localStorage.getItem("token"),
    'Access-Control-Allow-Origin': '*'
};

window.axios.interceptors.request.use(function(config) {
    return config;
}, function(error) {
    return Promise.reject(error);
});

window.axios.interceptors.response.use(function(response) {
    // app.$Progress.finish(); // finish when a response is received
    return response;
}, (error) => {
    console.log(error.response)
    if (error.response.status == 401)
    {
        router.push({ name: 'login' })
        Swal.fire(error.response.statusText, 'Harap login terlebih dahulu!');
    } else if (error.response.status == 422) {
      Swal.fire(error.response.status, error.response.data.message);
    }
    return Promise.reject(error);
});

export function removeHeaderToken() {
    delete axios.defaults.headers.common['Authorization']
}
