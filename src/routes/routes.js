import DashboardLayout from '@/views/Layout/DashboardLayout.vue';
import AuthLayout from '@/views/Pages/AuthLayout.vue';

import NotFound from '@/views/NotFoundPage.vue';

const routes = [
  {
    path: '/',
    redirect: 'absensi',
    component: DashboardLayout,
    children: [
      {
        path: '/absensi',
        name: 'absensi',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "demo" */ '../views/Absensi.vue'),
        meta: {
          breadcrumb: [
            {
              text: 'Absensi',
              href: '/absensi'
            }
          ]
        }
      },
      {
        path: '/table-absensi',
        name: 'table absensi',
        component: () => import(/* webpackChunkName: "demo" */ '../views/RegularTables.vue'),
        meta: {
          breadcrumb: [
            {
              text: 'Table Absensi',
              href: '/table-absensi'
            }
          ]
        }
      }
    ]
  },
  {
    path: '/',
    redirect: 'login',
    component: AuthLayout,
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/Login.vue')
      },
      {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/Register.vue')
      },
      { path: '*', component: NotFound }
    ]
  }
];

export default routes;
